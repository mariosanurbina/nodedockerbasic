# Basic NodeApp in Docker with a Volume

This code is useful for deploy a very basic node app with Docker. It makes use of a volume to share basic archives such as  package.json, node modules and index.js between the host and the container. 
So, in order to update dependecies and modules you would not need to update the container, just the folders in the host.

Basic use of the files

1. Create a local folder when you are going to have your folder
2. Download this folder of GitLab
3. Use the command docker-compose build
4. Use the command docker-compose up
5. Your basic server of node in Docker is ready