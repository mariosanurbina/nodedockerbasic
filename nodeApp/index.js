//code taken from: https://www.w3schools.com/nodejs/nodejs_http.asp

const http = require('http');

//create a server object:
http.createServer(function (req, res) {
  res.write('Hello World!'); //write a response to the client
  console.log("Listening on port 8080")
  res.end(); //end the response
}).listen(8080); //the server object listens on port 8080